from settings import settings
import urllib2
import urllib

import json
import math

class Search():
	def __init__(self):
		self.query = "q=*"
		self.results = []
		self.total = 0
		self.facets = {}
		self.current_page = 1
		self.query_obj = {}

	def build_query(self, **kwargs):
		#query, location, category, job_type, company
		self.query_obj = kwargs

		if kwargs.get("q"):
			self.query = "q="+urllib.quote(kwargs.get("q"))
		

		if kwargs.get("category"):
			self.query += "&+fq=+category:"+urllib.quote(kwargs.get("category"))

		if kwargs.get("location"):
			self.query += "&+fq=+location:"+urllib.quote(kwargs.get("location"))

		if kwargs.get("job_type"):
			self.query += "&+fq=+job_type:"+urllib.quote(kwargs.get("job_type"))

		if kwargs.get("company"):
			self.query += "&+fq=+company:"+urllib.quote(kwargs.get("company"))

		self.query += "&rows="+str(settings.get('results_per_page'))

		#only fetches matches of jobs posted from now till 60 days ago
		self.query += "&date:[NOW-60DAY%20TO%20NOW/HOUR]"

		# sort by date if no query string
		if not kwargs.get("q"):
			self.query += "&sort=date+desc"


		start = 0

		if kwargs.get("page"):
			page_int = int(kwargs.get("page"))
			self.current_page = page_int

			if page_int > 0:
				start = (page_int - 1) * settings.get("results_per_page")

		self.query += "&start="+str(start)
		# print start

		self.__add_facets()

		#also include pagination here

		# print self.query

		pass

	def __add_facets(self):
		self.query += "&facet=on&facet.field=location&facet.field=category&facet.field=job_type&facet.field=company"
		pass

	def fetch_results(self):
		url = settings.get("solr_query_server") %settings.get("country")

		url_with_query = url + self.query
		# print url_with_query
		response = urllib2.urlopen(url_with_query)

		# print response.read()

		obj_response = json.loads(response.read())

		# print(response.read())

		response.close()

		if obj_response["responseHeader"]["status"] == 0:
			self.total = obj_response["response"]["numFound"]
			self.results = obj_response["response"]["docs"]
			self.facets = self.__process_facets(obj_response["facet_counts"]["facet_fields"], self.query_obj)


		return self.results

		# raw contains the facet obj from solr and query is the query object of the current page
	def __process_facets(self,raw,query):

		print query
		# print raw
		facet = {}

		for key, value in raw.iteritems():
			facet[key] = {}

			items = value
			for i in range(0, len(items), 2):
				if items[i+1] > 0:
					facet[key.strip()][items[i]] = {}
					facet[key.strip()][items[i]]["count"] = items[i+1]

					#build the url path for each facet including its current query string and returning it to the first page
					query_obj = self.query_obj.copy()
					query_obj[key] = items[i]
					# print "here is:"+query_obj[key]
					query_obj["page"] = 1

					facet[key.strip()][items[i]]["url"] = urllib.urlencode(query_obj)


		print facet
		
		return facet

	def fetch_facets(self):
		return self.facets

	def get_total(self):
		return self.total

	def get_pagination(self, current_page):
		total_pages = self.total

		pagination_object = {}
		pagination_object["forward_pages"] = []
		pagination_object["backward_pages"] = []
		pagination_object["has_next"] = False
		pagination_object["has_prev"] = False
		pagination_object["current_page"] = str(current_page)

		num_pages = math.ceil(total_pages/float(settings.get("results_per_page")))

		pagination_object["total_pages"] = num_pages

		if current_page > 0:
			f_curr = current_page
			b_curr = current_page
			for i in range(3):
				if f_curr < num_pages:
					f_curr = f_curr + 1
					pagination_object["forward_pages"].append(str(f_curr))

				if b_curr > 1:
					b_curr = b_curr - 1
					pagination_object["backward_pages"].append(str(b_curr))
			if f_curr < num_pages:
				pagination_object["has_next"] = True

			if b_curr > 1:
				pagination_object["has_prev"] = True

		if len(pagination_object["backward_pages"]) > 0:
			pagination_object["backward_pages"].sort()
		
		print pagination_object
		
		return pagination_object

				



		pass

if __name__ == "__main__":
	search = Search()
	search.build_query(query="", location="Lagos")
	print search.fetch_results()

	# print search.fetch_facets()