from bottle import route, run, jinja2_template as template, TEMPLATE_PATH, static_file, request, redirect
from bottle import response as b_resp,abort

from search import Search
from settings import settings
from helpers import parse_url_to_query

import urllib, urllib2
import bottle
import json
import requests



TEMPLATE_PATH.append("./views")

index_template =  "theme1/index.html"
search_template = "theme1/search-results.html"
alert_template = "theme1/alert.html"
login_template = "theme1/login.html"

@route('/')
def home():
	url = settings.get("backend_server")+ "categories/"
	response = urllib.urlopen(url).read()

	categories = json.loads(response)
	
	# quote plus each category with space and change space to +
	categories = map(lambda cat: {"slug": urllib.quote_plus(cat["name"]), "name": cat["name"]}, categories)

	return template(index_template, settings=settings, categories=categories, meta_title=settings.get("meta_title"), meta_description=settings.get("meta_description"), q="", location="")


@route('/<url:re:.+\.html>')
def landing_page(url):
	q, location = parse_url_to_query(url)

	request.query["q"] = q
	request.query["location"] = location

	return search()
	# return url

@route('/alert')
def job_alert():
	meta_title = settings["pages"]["alert"]["meta_title"]
	meta_description = settings["pages"]["alert"]["meta_description"]

	return template(alert_template, meta_title=meta_title, meta_description=meta_description, settings=settings, q="", location="")

@route('/search')
def search():
	q = ""
	location = ""
	category = ""
	company = ""
	job_type = ""



	if request.query.get("q"):
		q = request.query.get("q")
	if request.query.get("location"):
		location = request.query.get("location")
	if request.query.get("company"):
		company = request.query.get("company")
	if request.query.get("category"):
		category = request.query.get("category")
	if request.query.get("job_type"):
		job_type = request.query.get("job_type")

	# if not q and not location and not company and not category and not job_type:
		# redirect("/")

	# register the query to the backend server
	if q:
		data = {
			"q": q,
			"country": settings.get("country")
		}
		log_query_url = settings.get("backend_server")+"register_query/?"+urllib.urlencode(data)
	#	print "log url is "+ log_query_url
		urllib2.urlopen(log_query_url).read()
	

	query_string = urllib.urlencode(request.query)


	page = 1
	if request.query.get("page"):
		if int(request.query.get("page")) > 0:
			page = int(request.query.get("page"))

	search = Search()
	search.build_query(q=q, location=location, category=category, job_type=job_type, company=company, page=page)
	jobs = search.fetch_results()
	facets = search.fetch_facets()
	
	total = search.get_total()

	#print jobs

	pagination = search.get_pagination(page)

	# print(pagination)

	# set meta title and meta description for search page
	meta_title = settings["pages"]["search"]["meta_title"]
	meta_description = settings["pages"]["search"]["meta_description"]

	if request.query.get("resp_type") == "json":
		b_resp.headers['Content-Type'] = 'text/json'
		return json.dumps({"total": total, "pagination":pagination, "jobs":jobs})

	return template(search_template, total=total, pagination=pagination, meta_title=meta_title, meta_description=meta_description, settings=settings, jobs=jobs, q=q, location=location, facets=facets, query_string=query_string, next=page+1, previous=page - 1 if page > 1 else 1)

@route('/job-alert')
def alert():
	email = request.query.get("email")
	keyword = request.query.get("keyword")
	location = request.query.get("location")

	country = settings.get("country")

	query_params = {
		"email": email,
		"keyword": keyword,
		"location": location,
		"country": country,
		"status": "Subscribed"
	}
	url = settings.get("backend_server") + "alert/?" + urllib.urlencode(query_params)
	
	response = urllib.urlopen(url).read()

	return "done"

@route('/sitemap.xml')
def sitemap():
	response = urllib2.urlopen(settings["backend_server"]+"display_sitemaps/?country="+settings["country"]).read()
	b_resp.headers['Content-Type'] = 'text/xml'

	sitemap = json.loads(response)

	

	urls_xml = ""
	for sp in sitemap:
		sitemp = """ 
			<url>
				<loc>%s</loc>
				<priority>%s</priority>
				<changefreq>%s</changefreq>
			</url>
		"""%(sp.get("path"), sp.get("priority"), sp.get("freq"))

		urls_xml += sitemp

	sitemap_xml = """
		<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
		%s
		</urlset>
	""" %(urls_xml)

	return template(sitemap_xml)

@route('/static/<filepath:path>')
def serve_static(filepath):
	print "I am in serve static"
	return static_file(filepath, root='./public')


@route('/xxxlogin')
def login():
	return template(login_template)

@route('/login/post', method='POST')
def login_api():
	url = settings.get("backend_server")+ "auth/"
	data = {
		"username": request.forms.get('username'),
		"password": request.forms.get('password')
	}
	response = urllib.urlopen(url+'?'+urllib.urlencode(data))
	if response.code == 200:
		return response.read()
	else:
		abort(404)


@route('/login/check', method='POST')
def login_check():
	url = settings.get("backend_server")+ "auth/check/"
	data = {
		"sesid": request.get_cookie('sessionid'),
	}
	response = urllib.urlopen(url+'?'+urllib.urlencode(data))
	if response.code == 200:
		return response.read()
	else:
		abort(404)


@route('/record/delete', method='POST')
def record_delete():
	url = settings.get("backend_server")+ "auth/record/delete/"
	data = {
		"sesid": request.get_cookie('sessionid'),
		"pk": request.forms.get('pk')
	}
	response = urllib.urlopen(url+'?'+urllib.urlencode(data))
	if response.code == 200:
		return response.read()
	else:
		abort(404)

@route('/record/edit', method='POST')
def record_edit():
	url = settings.get("backend_server")+ "auth/record/edit/"
	data = {
		"sesid": request.get_cookie('sessionid'),
		"pk": request.forms.get('pk'),
	}
	response = urllib.urlopen(url+'?'+urllib.urlencode(data))
	if response.code == 200:
		return response.read()
	else:
		abort(404)


@route('/record/save', method='POST')
def record_edit():
	url = settings.get("backend_server")+ "auth/record/edit/"
	data = {}
	for item in request.forms:
		data.update({item:request.forms.get(item)})
	r = requests.post(url, data=data)
	if r.status_code == 200:
		return "ok"
	elif r.status_code == 400:
		b_resp.content_type = 'application/json'
		b_resp.status = 400
		return json.dumps(r.json())
