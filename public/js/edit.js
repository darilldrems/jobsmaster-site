function del_record(id){
    if(confirm('Are you sure?')){
        $.ajax({
            type: "POST",
            url: '/record/delete',
            data: {
                'pk':id
            },
            success: function(data){
                location.reload();
            },
            error:  function(resp) {
                alert('something went wrong')
            }
        });
    }
}

var current_editing = 0;
function edit_record(id){
    $.ajax({
        type: "POST",
        url: '/record/edit',
        data: {
            'pk':id
        },
        success: function(data){
            current_editing = id;
            $('#edit_form table').html(data);
            $('#edit').show()
        },
        error:  function(resp) {
            alert('something went wrong')
        }
    });
}

function save_record(){
    var dat=$('#edit_form form').serialize() + '&pk='+current_editing;
    $.ajax({
        type: "POST",
        url: '/record/save',
        data: dat,
        success: function(data){
            location.reload();
        },
        error:  function(resp) {
            $('.error').remove();
            var errors = JSON.parse(resp.responseText);
            for (error in errors) {
                err=errors[error];
                $('#id_'+error).after('<div class="error">'+err+'</div>');

            }
        }
    });
}

function edit_records(){
    $( ".result-item" ).each(function( index ) {
          id= $(this).find('h4 a').attr('data-id');
          $(this).find('h4 a').after(' #'+id +' <div class="dropdown"><ul><li onclick="edit_record('+id+')">Edit</li><li onclick="del_record('+id+')">Delete</li></ul></div>')
    });

}

function edit(){
        $.ajax({
            type: "POST",
            url: '/login/check',
            success: function(data){
                edit_records()
            },
            error:  function(resp) {

            }
        });
}


