// alert("hello")
var alertMixin = {
	validateEmail: function validateEmail(email) {
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		return re.test(email);
	},
	submitEmail: function submitEmail() {
		var email = React.findDOMNode(this.refs.email).value.trim();
		if (this.validateEmail(email)) {
			var q = $("input[name=q]").val() || React.findDOMNode(this.refs.q).value.trim();
			var location = $("input[name=location]").val() || React.findDOMNode(this.refs.location).value.trim();
			// alert(q)
			if (this.props.required) {
				if (!q) {

					this.setState({ messageColor: "red" });
					this.setState({ message: "Keyword is required" });
					return;
				}
			}

			$.ajax({
				url: "/job-alert?email=" + email + "&country=" + this.props.country + "&keyword=" + q + "&location=" + location,
				dataType: "json",
				cache: false,
				statusCode: {
					200: (function () {
						React.findDOMNode(this.refs.email).value = "";
						this.setState({ message: "You have been subscribed" });
						this.setState({ messageColor: "#398439" });
					}).bind(this)
				}
			});
		} else {
			this.setState({ messageColor: "red" });
			this.setState({ message: "Your email is invalid" });
		}
	}
};

var AlertBox = React.createClass({
	displayName: "AlertBox",

	mixins: [alertMixin],
	getInitialState: function getInitialState() {
		return {
			message: "",
			messageColor: ""
		};
	},
	render: function render() {
		var messageStyle = {
			color: this.state.messageColor
		};

		return React.createElement(
			"div",
			{ className: "email-alert" },
			React.createElement(
				"h5",
				{ className: "text-center" },
				"Create Email Job Alert"
			),
			React.createElement(
				"span",
				{ style: messageStyle },
				this.state.message
			),
			React.createElement(
				"div",
				{ className: "form-group" },
				React.createElement(
					"label",
					null,
					"What ",
					React.createElement(
						"span",
						null,
						"(job keywords)"
					)
				),
				React.createElement("input", { ref: "q", type: "text", className: "form-control input-sm" })
			),
			React.createElement(
				"div",
				{ "class": "form-group" },
				React.createElement(
					"label",
					null,
					"Where ",
					React.createElement(
						"span",
						null,
						"(city, state)"
					)
				),
				React.createElement("input", { ref: "location", type: "text", className: "form-control input-sm" })
			),
			React.createElement(
				"div",
				{ "class": "form-group" },
				React.createElement(
					"label",
					null,
					"Email"
				),
				React.createElement("input", { ref: "email", type: "email", className: "form-control input-sm" })
			),
			React.createElement(
				"div",
				{ "class": "form-button text-center" },
				React.createElement(
					"p",
					null,
					" "
				),
				React.createElement(
					"button",
					{ onClick: this.submitEmail, type: "submit", className: "btn btn-primary" },
					"Create Alert"
				)
			)
		);
	}
});

var Subscribe = React.createClass({
	displayName: "Subscribe",

	mixins: [alertMixin],
	getInitialState: function getInitialState() {
		return {
			message: "Get new jobs for this search by email",
			messageColor: ""
		};
	},

	render: function render() {
		var messageStyle = {
			color: this.state.messageColor
		};
		return React.createElement(
			"div",
			{ className: "subscriptions" },
			React.createElement(
				"p",
				{ style: messageStyle },
				React.createElement("i", { className: "fa fa-envelope-o" }),
				" ",
				this.state.message
			),
			React.createElement(
				"label",
				null,
				"My email:"
			),
			React.createElement("input", { type: "email", ref: "email", className: "form-control" }),
			React.createElement(
				"button",
				{ onClick: this.submitEmail, className: "btn btn-sm btn-primary text-center" },
				"Activate"
			),
			React.createElement(
				"p",
				null,
				"You can cancel email alerts at any time"
			)
		);
	}
});

var TimeBox = React.createClass({
	displayName: "TimeBox",

	render: function render() {

		var time_ago = moment(this.props.date).fromNow();

		return React.createElement(
			"span",
			null,
			"posted ",
			time_ago
		);
	}
});