// alert("hello")
var alertMixin = {
	validateEmail: function(email){
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    	return re.test(email);
	},
	submitEmail: function(){
		var email = React.findDOMNode(this.refs.email).value.trim();
		if(this.validateEmail(email)){
			if(React.findDOMNode(this.refs.q)){
				var q = $("input[name='q']").val() || React.findDOMNode(this.refs.q).value.trim();
				var location = $("input[name='location']").val()|| React.findDOMNode(this.refs.location).value.trim();
			}else{
				var q = $("input[name='q']").val();
				var location = $("input[name='location']").val();
			}
			
			
			// alert(q)
			if(this.props.required){
				if(!q){
					
					this.setState({messageColor: "red"})
					this.setState({message: "Keyword is required"})
					return 
				}
			}

			$.ajax({
				url: "/job-alert?email="+email+"&country="+this.props.country+"&keyword="+q+"&location="+location,
				dataType: "json",
				cache: false,
				statusCode: {
				    200: function() {
				      React.findDOMNode(this.refs.email).value = "";
						this.setState({message: "You have been subscribed"});
						this.setState({messageColor: "#398439"});
				    }.bind(this)
				  }
			})
		}else{
			this.setState({messageColor: "red"})
			this.setState({message: "Your email is invalid"})
		}
	}
};

var AlertBox = React.createClass({
	mixins: [alertMixin],
	getInitialState: function(){
		return {
			message: "",
			messageColor: ""
		}
	},
	render: function(){
		var messageStyle = {
				color: this.state.messageColor
			}

		return (
			
			<div className="email-alert">
                        <h5 className="text-center">Create Email Job Alert</h5>
                        <span style={messageStyle}>{this.state.message}</span>
                        <div className="form-group">
                            <label>What <span>(job keywords)</span></label>
                            <input ref="q" type="text" className="form-control input-sm" />
                        </div>
                        <div class="form-group">
                            <label>Where <span>(city, state)</span></label>
                            <input ref="location" type="text" className="form-control input-sm" />
                        </div>
                        
                        <div class="form-group">
                            <label>Email</label>
                            <input ref="email" type="email" className="form-control input-sm" />
                        </div>
                        <div class="form-button text-center">
                        	<p>fdzfvdzf </p>
                            <button onClick={this.submitEmail} type="submit" className="btn btn-primary">Create Alert</button>
                        </div>
                    </div>


			)
	}
});


var Subscribe = React.createClass({
	mixins: [alertMixin],
	getInitialState: function(){
		return {
			message:"Get new jobs for this search by email",
			messageColor:""
		}
	},
	
	render: function(){
		var messageStyle = {
			color: this.state.messageColor
		}
		return (
			<div className="subscriptions">
	            <p style={messageStyle} ><i className="fa fa-envelope-o"></i> {this.state.message}</p>
	            <label>My email:</label>
	            <input type="email" ref="email" className="form-control" />
	            <button onClick={this.submitEmail} className="btn btn-sm btn-primary text-center">Activate</button>
	            <p>You can cancel email alerts at any time</p>
	        </div>


			)
		
	}
})


var TimeBox = React.createClass({

	render: function(){

		var time_ago = moment(this.props.date).fromNow()

		return (
			<span>posted {time_ago}</span>
			)
	}
})