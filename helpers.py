# from settings import settings

# def create_pagination(total, current_page, query_string):
# 	total_pages = total/settings.results_per_page

# 	if current_page > total_pages:
		
# 	pass

import re

def parse_url_to_query(url):
	pattern = "^([a-zA-Z]+?)-jobs(-in)?-?([a-zA-Z]+)?(-state)?\.html$"
	# pattern = "h"

	# regex = re.compile(pattern, re.IGNORECASE)
	# print url
	q = None
	location = None

	if "in" in url:
		result = re.sub(pattern, r"\1, \3", url)
		q = result.split(",")[0]
		location = result.split(",")[1]
	else:
		result = re.sub(pattern, r"\1", url)
		q = result
	
	
	return q, location


if __name__ == "__main__":
	print parse_url_to_query("hotel-jobs-in-abuja-state.html")